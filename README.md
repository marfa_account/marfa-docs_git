# MARFA #

This is the landing page for MARFA.  Please go to the [wiki][wikilink] to see a summary of the capabilities and different versions available. The User's Manual is found in the [downloads page][downloads_page].

[wikilink]: https://bitbucket.org/marfa_account/marfa-docs/wiki/Home
[downloads_page]: https://bitbucket.org/marfa_account/marfa-docs/downloads/

Please contact Amphos 21 (marfa@amphos21.com) if you have any requests.

## LICENSE

MARFA is distributed under the terms of the BSD 3-clause license.